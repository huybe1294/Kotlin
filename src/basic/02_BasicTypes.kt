package basic

class BasicTypes {

    /** @Numbers:
     * Double: 64  default: 123.5
     * Float: 32   are tagged by f or F: 123.5f, 123.5F
     * Long: 64    are tagged by L
     * Int: 32
     * Short: 16
     * Byte: 8
     *
     * @Note: characters are not numbers in kotlin
     */
    var a: Double = 123.5
    var b: Float = 123.5f
    var c: Long = 123L
    var d: Int = 12
    var e: Short = 12
    var f: Byte = 1


    /**@Note: Explicit Conversions
     * we need use explicit conversions to widen numbers
     * toByte()
     * toShort()
     * toInt()
     * toLong()
     * toFloat()
     * tyDouble()
     * toChar()
     */
    private var h: Byte = 5
    //    var i1:Int=a1 //Error
    var i: Int = h.toInt() // OK: explicitly widened

    /**@Characters
     *
     */
    var k:Char='c'

    /**@Booleans
     * ||  lazy disjunction
     * && lazy conjunction
     * ! negation
     */

    /**@Arrays
     *Arrays in Kotlin are represented by the Array class, that has get and set functions
     */
    var arrX:IntArray= intArrayOf(1,2,3)
    var arrY=Array(5,{i->(i*i).toString()}) // Creates an Array<String> with values ["0", "1", "4", "9", "16"]

    /**@String
     *Strings are represented by the type String. Strings are immutable.
     * String Templates
     * val i = 10
     * println("i = $i") // prints "i = 10"
     */


    constructor(){
    }
}