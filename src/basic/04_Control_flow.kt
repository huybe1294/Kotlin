package basic

class ControlFlow {
    /**
     * @if Expression
     */
    var a: Int = 5
    var b: Int = 3



    constructor() {

        // Traditional usage
        var max = a
        if (a < b) max = b

        //With else
        if (a > b) {
            max = a
        } else {
            max = b
        }

        //as expression
        val max1 = if (a > b) a else b
    }

    /**
     *@When expression
     * replace the Switch
     */

    fun whenExpression() {
        //in the simplest form
        println("\n enter n: ")
        val x = readLine()
        if (x != null) {
            when (x.toInt()) {
                1 -> print("1")
                2 -> print("2")
                else -> {
                    print("x is neither 1 nor 2")
                }
            }
        }

        //We can also check a value for being in or !in a range or a collection:
        if (x != null) {
            when (x.toInt()) {
                in 1..10 -> print("x is in the range")
                !in 1..20 -> print("x is outside the range")
                else -> print("none of the above")
            }
        }

        /**
         * @For_Loop
         */

        //normal
        for (x in 1..10) {
            println(x)
        }

        //foreach
        val arrInt = arrayOf(1, 2, 4)
        for (item in arrInt) {
            println(item)
        }

        //indices
        val array1 = arrayOf("a", "b", "c")
        for (i in array1.indices) {
            println(array1[i])
        }

        //withIndex
        val array = arrayOf("a", "b", "c")
        for ((index, value) in array.withIndex()) {
            println("the element at $index is $value")
        }

        /**@While_Loop work as usual
         */

        /**
         * @Break_and_Continue work as usual
         */
    }
}