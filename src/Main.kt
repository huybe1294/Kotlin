import basic.ControlFlow
import basic.Helloword;
import class_object.C
import orther.Ranges

fun main(args: Array<String>) {
//    val helloword = Helloword() //01: hello word
//
//    val ranges = Ranges()
//
//    val controlFlow = ControlFlow()
//    controlFlow.whenExpression()

//    val inter=C()
//    inter.foo()

//    var ar: Array<IntArray> = Array(10, { IntArray(5) })
//    for (i in ar.indices) {
//        for (j in ar[i].indices) {
//            println("$i  $j")
//        }
//    }

    val lazyValue:String by lazy {
        println("computed")
        "sad"
    }

    println(lazyValue)
}
