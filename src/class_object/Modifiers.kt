package class_object

/*
* There are 4 visibility modifiers : private, protected, internal, public
* default is public
*/

class Modifiers {

    /** @Packages
     * private: only be inside the file
     * internal: everywhere in the same module
     * protected: is not available for top-level declaration
     */

    /**@Class_and_Interface
     * private: this class only
     * protected: the same private + subclass
     * internal: inside this module
     * public: everywhere
     */

    //local variable, functions,
}