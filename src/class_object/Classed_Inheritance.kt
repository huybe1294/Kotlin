package class_object

/**
 * @class
 */
class Invoice {}

/**
 * @constructor
 */
// primary constructor (phương thức khơi tạo chính)
// cannot contain any code
// đi sau tên lớp và các tham số tùy chọn
class Person constructor(firstName: String)

// initializer blocks (khối khởi tạo)
// chạy trước constructor
class InitDemo {
    init {
        println("init")
    }
}

//secondary constructor
//nếu đã có 'primary constructor' thì mỗi constructor thứ cấp phải ủy nhiệm cho 'primary constructor' qua từ khóa 'this'
class Secondary(var name: String) {
    constructor(name: String, age: Int) : this(name) {}
}

/**
 * @Inheritance
 *
 */


