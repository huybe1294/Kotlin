package class_object

//có thể khai báo thuộc tính trong interface, các thuộc tính là abstract
//có thể triển khai phương thức
//không thể lưu giữ trang thái
interface A {
    var a: Int
    fun foo() {
        println("A")
    }
}

interface B {
    fun foo() {
        println("B")
    }

    fun bar()
}

class C : A,B {
    override fun bar() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun foo() {
        super<A>.foo()
        super<B>.foo()
    }

    override var a: Int=1
}