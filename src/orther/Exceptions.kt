package orther

import java.lang.Integer.parseInt

class Exceptions {
    init {
        /**
         * @Try_exception
         */

        try {
            //some code
        } catch (e: Exception) {
            //handler
        } finally {
            //optional finally block
        }

        //Try là 1 biểu thức, nghĩa là nó có thể có  một giá trị trả về
        val a: Int? = try {
            parseInt("a")
        } catch (e: NumberFormatException) {
            null
        }

        /**
         * @Eg
         */
        div(2, 6)
    }

    fun div(a: Int, b: Int): Int {
        if (b == 0) throw Exception("Mẫu =0")
        return a / b;
    }

}